extends KinematicBody2D

var dir = Vector2()

func _ready():
	pass

func _process(delta):
	pass

func _physics_process(delta):
	if get_parent().has_method("MoveChar"):
		dir = get_parent().dir
		print(dir)
	move_and_slide(dir)