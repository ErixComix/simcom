extends "Char.gd".Char

export var override_traits = Dictionary() # Inspector trait overrides list
export var char_name = "Name"
export var trait_value_default = 0 # Basically a constant, but might be fun to alter based on mood!

func _ready():
	char_name = name
	ImportTraits()

# Load char_traits from external file
func ImportTraits():
	var char_traits = Dictionary()
	var charfile = File.new()
	var filepath = "res://characters/" + char_name.to_lower() + ".char"
	
	charfile.open(filepath, File.READ)
	char_traits = ParseFile(charfile)
	charfile.close()
	for i in char_traits.keys():
		if not override_traits.has(i): # Check trait isn't overridden in the inspector
			traits[i] = char_traits[i]
	return

# Parse charfile
func ParseFile(charfile):
	var char_traits = Dictionary() # Temporary list of traits
	var traitsarray = Array()
	
	while charfile.get_position() < charfile.get_len():
		var readtrait = charfile.get_line() # Start operating on the next trait definition
		if not readtrait.begins_with("\n") and not readtrait.begins_with("#"): # Skip empty and commented lines
			readtrait = readtrait.replace(" ", "").replace("\t", "").replace("\n", "") # Strip out any whitespace
			if not readtrait.find("#") == -1:
				readtrait = readtrait.left("#") # Strip out any comments
			var key
			var value = trait_value_default
			if readtrait.find("="): # Check if a value is provided
				key = readtrait.left(readtrait.find("="))
				value = readtrait.right(readtrait.find("=") + 1).to_float()
			else: # No value was provided
				key = readtrait # Treat entire string as key, just keep using default value
			char_traits[key] = value # Add it to the list!
	return char_traits

func ExportTraits():
	var char_traits = Dictionary()
	var charfile = File.new()
	var filepath = "res://characters/" + char_name.to_lower() + ".char"
	
	charfile.open(filepath, File.WRITE)
	char_traits = ParseFile(charfile)
	for i in char_traits.size():
		charfile.store_line(char_traits.keys()[i], " = ", char_traits.values()[i])
	return