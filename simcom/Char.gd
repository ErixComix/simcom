class Char extends Node:
		
	#######################
	### Trait Variables ###
	#######################
	
	var traits = {
	"likesLeft":0,
	"likesRight":0,
	"social":0
	}
	
	var dir = Vector2()
	
	func _ready():
		pass
	
	func _process(delta):
		if traits["likesLeft"] > 0:
			MoveChar("left", traits["likesLeft"])
		if traits["likesRight"] > 0:
			MoveChar("right", traits["likesRight"])
	
	
	################
	### Movement ###
	################
	
	#TODO: Might not need to handle any of this in the logic...
	
	func MoveChar(moveDir, moveSpeed):
		if moveDir == "left":
			dir.x -= moveSpeed
		elif moveDir == "right":
			dir.x += moveSpeed
	
	################
	### Thinking ###
	################
	
	func CheckTrait(trait):
		pass
	
	func UpdateTrait(trait):
		pass